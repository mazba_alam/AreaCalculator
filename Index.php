<?php
include_once 'vendor/autoload.php';

use Pondit\Calculator\AreaCalculator\Rectangle;
use Pondit\Calculator\AreaCalculator\Square;
use Pondit\Calculator\AreaCalculator\Triangle;
use Pondit\Calculator\AreaCalculator\Circle;

$rectangle= new Rectangle();
var_dump($rectangle);

$square=new Square();
var_dump($square);

$triangle=new Triangle();
var_dump($triangle);

$circle=new Circle();
var_dump($circle);